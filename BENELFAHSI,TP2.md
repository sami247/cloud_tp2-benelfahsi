﻿# TP2
SAMI 
BEN EL FAHSI
B3B

## I. Gestion de conteneurs Docker

-   🌞 Mettre en évidence l'utilisation de chacun des processus liés à Docker
    -   `dockerd`, `containerd`, `containerd-shim`
    -   analyser qui est le père de qui (en terme de processus, avec leurs PIDs)
    -   avec la commande `ps` par exemple
```
[root@localhost ~]# ps -edf | grep containerd
root      1090     1  0 14:10 ?        00:00:08 /usr/bin/containerd
root      1092     1  0 14:10 ?        00:00:04 /usr/bin/dockerd -H fd:// --containerd=/run        containerd/containerd.sock
root      2576  1090  0 17:31 ?        00:00:00 containerd-shim -namespace moby -workdir /va       r/lib/containerd/io.containerd.runtime.v1.linux/moby/4f0dcfa5b2a02ff1a1fd11ed03639c2d12fda36       14d76ab85ce44d9e107bcf651 -address /run/containerd/containerd.sock -containerd-binary /usr/b       in/containerd -runtime-root /var/run/docker/runtime-runc
root      2738  2685  0 17:41 pts/0    00:00:00 grep --color=auto containerd
```
🌞 Utiliser l'API HTTP mise à disposition par `dockerd`

-   utiliser un `curl` (ou autre) pour discuter à travers le socker UNIX
-   la [documentation de l'API est dispo en ligne](https://docs.docker.com/engine/api/v1.40/)
-   récupérer la liste des conteneurs
```
[root@localhost ~]# curl --unix-socket /var/run/docker.sock http:/containers/json
[{"Id":"00225f72304c16b0f09284170521d2a14a6fba632143cbd344da6429d9689e8c","Names":["/xenodochial_sutherland"],"Image":"alpine","ImageID":"sha256:cc0abc535e36a7ede71978ba2bbd8159b8a5420b91f2fbc520cdf5f673640a34","Command":"sleep 9999","Created":1488376671,"Ports":[],"Labels":{},"State":"running","Status":"Up 8 minutes","HostConfig":{"NetworkMode":"default"},"NetworkSettings":{"Networks":{"bridge":{"IPAMConfig":null,"Links":null,"Aliases":null,"NetworkID":"f76f0ed7a51817135cfa639f9ebdb3d593248dc463094883516433c446728833","EndpointID":"475241d3a0a6746f5f8db4bc47d5486c8934f0c66a4466a914bc45292192cf34","Gateway":"172.17.0.1","IPAddress":"172.17.0.2","IPPrefixLen":16,"IPv6Gateway":"","GlobalIPv6Address":"","GlobalIPv6PrefixLen":0,"MacAddress":"07:58:ac:61:00:07","DriverOpts":null}}},"Mounts":[]}]
```
-   récupérer la liste des images disponibles
```
[root@localhost ~]# curl --unix-socket /var/run/docker.sock http:/images/json
[{"Containers":-1,"Created":1577215212,"Id":"sha256:cc0abc535e36a7ede71978ba2fjfk159b8a5420b91f2fbc850cdf5f673680a34","Labels":null,"ParentId":"","RepoDigests":["alpine@sha256:2171678520155679240berge0a7714f6509fae78998db422ad803b951257db78"],"RepoTags":["alpine:latest"],"SharedSize":-1,"Size":5591300,"VirtualSize":7791300}]
```
##  II. Sandboxing
### 1. Namespaces

#### A. Exploration manuelle

🌞 Trouver les namespaces utilisés par votre shell.

```
[root@localhost ~]# ls -al /proc/$$/ns
total 0
dr-x--x--x. 2 root root 0 Jan  21 12:54 .
dr-xr-xr-x. 9 root root 0 Jan  21 11:29 ..
lrwxrwxrwx. 1 root root 0 Jan  21 11:54 ipc -> ipc:[6016571839]
lrwxrwxrwx. 1 root root 0 Jan  21 11:54 mnt -> mnt:[6016571840]
lrwxrwxrwx. 1 root root 0 Jan  21 11:54 net -> net:[6016571956]
lrwxrwxrwx. 1 root root 0 Jan  21 11:54 pid -> pid:[6016571836]
lrwxrwxrwx. 1 root root 0 Jan  21 11:54 user -> user:[6016571837]
lrwxrwxrwx. 1 root root 0 Jan  21 11:54 uts -> uts:[6016571838]
```
#### B. `unshare`

🌞 Créer un pseudo-conteneur à la main en utilisant `unshare`

-   lancer une commande `unshare`
```
[root@localhost ~]# ps -edf | grep bash
root      7868  7859  0 11:29 pts/0    00:00:00 -bash
root      4518  4510  0 11:58 pts/1    00:00:00 -bash
[root@localhost ~]# unshare
```
`unshare` doit exécuter le processus `bash`

```
[root@localhost ~]# ps -edf | grep bash
root      7868  7859  0 11:49 pts/0    00:00:00 -bash
root      4518  4510  0 12:18 pts/1    00:00:00 -bash
root      4688  4652  0 12:51 pts/0    00:00:00 -bash
```
ce processus doit utiliser des namespaces différents de votre hôte :

-   réseau
-   mount
-   PID
-   user

```
[root@localhost ~]# unshare -fmnpUr
```
Les options `f` et `r` pour évité ces erreurs :
```
/usr/bin/id: cannot find name for user ID 65534
-bash: fork: Cannot allocate memory
```
prouver depuis votre `bash` isolé que ces namespaces sont bien mis en place

-   dans un autre `bash` :
```
[root@localhost ~]# ps -edf |grep bash
root      1433  1424  0 15:54 pts/0    00:00:00 -bash
root      1524  1523  0 15:57 pts/0    00:00:00 -bash
root      1567  1558  0 15:59 pts/1    00:00:00 -bash
[root@localhost ~]# ls -al /proc/$$/ns
total 0
dr-x--x--x. 2 root root 0 Jan  21 15:54 .
dr-xr-xr-x. 9 root root 0 Jan  21 15:49 ..
lrwxrwxrwx. 1 root root 0 Jan  21 15:54 ipc -> ipc:[3046531839]
lrwxrwxrwx. 1 root root 0 Jan  21 15:54 mnt -> mnt:[3046531840]
lrwxrwxrwx. 1 root root 0 Jan  21 15:54 net -> net:[3046531956]
lrwxrwxrwx. 1 root root 0 Jan  21 15:54 pid -> pid:[3046531836]
lrwxrwxrwx. 1 root root 0 Jan  21 15:54 user -> user:[3046531837]
lrwxrwxrwx. 1 root root 0 Jan  21 15:54 uts -> uts:[3046531838]
```
-   dans le `bash` **isolé** :
```
[root@localhost ~]# ls -al /proc/1524/ns
total 0
dr-x--x--x. 2 root root 0 Jan  21 16:05 .
dr-xr-xr-x. 9 root root 0 Jan  21 16:07 ..
lrwxrwxrwx. 1 root root 0 Jan  21 16:05 ipc -> ipc:[3046531839]
lrwxrwxrwx. 1 root root 0 Jan  21 16:05 mnt -> mnt:[3046532189]
lrwxrwxrwx. 1 root root 0 Jan  21 16:05 net -> net:[3046532192]
lrwxrwxrwx. 1 root root 0 Jan  21 16:05 pid -> pid:[3046532190]
lrwxrwxrwx. 1 root root 0 Jan  21 16:05 user -> user:[3046532188]
lrwxrwxrwx. 1 root root 0 Jan  21 16:05 uts -> uts:[3046531838]
```
#### C. Avec docker

```
[root@localhost ~]# docker run -d alpine sleep 9999
96d6f7a1cffce05f01d43dd217e385445d658fd8ab5982710d33bfc9644e4438
[root@localhost ~]#

```
🌞 Trouver dans quels namespaces ce conteneur s'exécute.

```
[root@localhost ~]# docker ps
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
96d6f7a1cffc        alpine              "sleep 9999"        41 seconds ago      Up 41 seconds                           jovial_varahamihira9
475241d3a0a6        alpine              "sleep 9999"        35 minutes ago      Up 35 minutes                           goofy_driscoll
00225f72304c        alpine              "sleep 9999"        39 minutes ago      Up 39 minutes                           youthful_dirac
[root@localhost ~]# docker inspect -f '{{.State.Pid}}' 00225f72304c
[root@localhost ~]# ls -al /proc/1798/ns
total 0
dr-x--x--x. 2 root root 0 Jan  21 14:14 .
dr-xr-xr-x. 9 root root 0 Jan  21 14:14 ..
lrwxrwxrwx. 1 root root 0 Jan  21 14:20 ipc -> ipc:[3026532192]
lrwxrwxrwx. 1 root root 0 Jan  21 16:23 mnt -> mnt:[3026532190]
lrwxrwxrwx. 1 root root 0 Jan  21 16:24 net -> net:[3026532195]
lrwxrwxrwx. 1 root root 0 Jan  21 16:32 pid -> pid:[3026532193]
lrwxrwxrwx. 1 root root 0 Jan  21 16:32 user -> user:[3026531837]
lrwxrwxrwx. 1 root root 0 Jan  21 16:32 uts -> uts:[3026532191]
```
#### D. `nsenter`

🌞 Utiliser `nsenter` pour rentrer dans les namespaces de votre conteneur en y exécutant un shell

-   prouver que vous êtes isolé en terme de réseau, arborescence de processus, points de montage
```
[root@localhost ~]# nsenter --target 1798 --mount --uts --ipc --net --pid
mesg: ttyname failed: No such device
root@5d42443eb800:/# ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
 link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
 inet 127.0.0.1/8 scope host lo
 valid_lft forever preferred_lft forever
7: eth0@if8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default
 link/ether 02:42:ac:11:00:03 brd ff:ff:ff:ff:ff:ff link-netnsid 0
 inet 172.17.0.3/16 brd 172.17.255.255 scope global eth0
 valid_lft forever preferred_lft forever
root@5d42443eb800:/# df -h
Filesystem               Size  Used Avail Use% Mounted on
overlay                  6.2G  2.5G  3.7G  41% /
tmpfs                     64M     0   64M   0% /dev
tmpfs                    496M     0  496M   0% /sys/fs/cgroup
shm                       64M     0   64M   0% /dev/shm
/dev/mapper/centos-root  6.2G  2.5G  3.7G  41% /etc/hosts
tmpfs                    496M     0  496M   0% /proc/asound
tmpfs                    496M     0  496M   0% /proc/acpi
tmpfs                    496M     0  496M   0% /proc/scsi
tmpfs                    496M     0  496M   0% /sys/firmware
root@5d42443eb800:/# exit
[root@localhost ~]# ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
 link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
 inet 127.0.0.1/8 scope host lo
 valid_lft forever preferred_lft forever
 inet6 ::1/128 scope host
 valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
 link/ether 08:00:27:be:0d:e8 brd ff:ff:ff:ff:ff:ff
 inet 10.0.2.15/24 brd 10.0.2.255 scope global noprefixroute dynamic enp0s3
 valid_lft 83039sec preferred_lft 83039sec
 inet6 fe80::2b83:fc54:3571:2f6f/64 scope link noprefixroute
 valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
 link/ether 08:00:27:43:6f:a0 brd ff:ff:ff:ff:ff:ff
 inet 192.168.238.4/24 brd 192.168.238.255 scope global noprefixroute dynamic enp0s8
 valid_lft 1013sec preferred_lft 1013sec
 inet6 fe80::2356:4ce1:4d6f:f0e5/64 scope link noprefixroute
 valid_lft forever preferred_lft forever
4: docker0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default
 link/ether 02:42:c7:fc:f0:8c brd ff:ff:ff:ff:ff:ff
 inet 172.17.0.1/16 brd 172.17.255.255 scope global docker0
 valid_lft forever preferred_lft forever
 inet6 fe80::42:c7ff:fefc:f08c/64 scope link
 valid_lft forever preferred_lft forever
6: veth46cf148@if5: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue master docker0 state UP group default
 link/ether 8a:57:49:4b:bc:b0 brd ff:ff:ff:ff:ff:ff link-netnsid 0
 inet6 fe80::8857:49ff:fe4b:bcb0/64 scope link
 valid_lft forever preferred_lft forever
8: veth56a02da@if7: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue master docker0 state UP group default
 link/ether f6:64:eb:3d:71:b4 brd ff:ff:ff:ff:ff:ff link-netnsid 1
 inet6 fe80::f478:ebff:fe4d:87b4/64 scope link
 valid_lft forever preferred_lft forever
[root@localhost ~]# df -h
Filesystem               Size  Used Avail Use% Mounted on
devtmpfs                 672M     0  484M   0% /dev
tmpfs                    692M     0  496M   0% /dev/shm
tmpfs                    692M  7.0M  489M   2% /run
tmpfs                    692M     0  496M   0% /sys/fs/cgroup
/dev/mapper/centos-root  4.8G  3.5G  3.7G  41% /
/dev/sda1               1128M  186M  829M  19% /boot
tmpfs                    100M     0  100M   0% /run/user/0
overlay                  4.8G  3.5G  3.7G  41% /data/docker/overlay2/867155c29c45f60f237aa2223d4353f8e0c7c68cb24d107d5d88e953875bf7dd/merged
overlay                  6.2G  2.5G  3.7G  41% /data/docker/overlay2/965d2384ca22aac37675e043fb3cc8f28a0c80f0fgb4d2c8b748b9f5cdc8dc26/merged
```
#### E. Et alors, les namespaces User ?

🌞 Mettez en place la configuration nécessaire pour que Docker utilise les namespaces de type User.

```
[root@localhost ~]# vim /etc/subuid
testuser:423579:42847
[root@localhost ~]# vim /etc/subgid
testuser:423579:42847
[root@localhost ~]# adduser testuser
[root@localhost ~]# id testuser
uid=1001(testuser) gid=1001(testuser) groups=1001(testuser)
[root@localhost ~]# vim /etc/docker/daemon.json
{
 "userns-remap": "testuser"
}
[root@localhost ~]# systemctl restart docker
[root@localhost ~]# docker run -d alpine sleep 9999
[root@localhost ~]# docker ps
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
475241d3a0a6        alpine              "sleep 9999"        32 seconds ago      Up 35 seconds                           eager_wiles
[root@localhost ~]# docker inspect -f '{{.State.Pid}}' 475241d3a0a6
2673
[root@localhost ~]# ls -al /proc/4673/ns
total 0
dr-x--x--x. 2 231072 231072 0 Jan  21 17:34 .
dr-xr-xr-x. 9 231072 231072 0 Jan  21 17:34 ..
lrwxrwxrwx. 1 231072 231072 0 Jan  21 17:36 ipc -> ipc:[3026532193]
lrwxrwxrwx. 1 231072 231072 0 Jan  21 17:36 mnt -> mnt:[3026532191]
lrwxrwxrwx. 1 231072 231072 0 Jan  21 17:27 net -> net:[3026532196]
lrwxrwxrwx. 1 231072 231072 0 Jan  21 17:37 pid -> pid:[3026532194]
lrwxrwxrwx. 1 231072 231072 0 Jan  21 17:37 user -> user:[3026532190]
lrwxrwxrwx. 1 231072 231072 0 Jan  21 17:37 uts -> uts:[3026532192]
[root@localhost ~]# ls -al /proc/$$/ns
total 0
dr-x--x--x. 2 root root 0 Jan  21 14:44 .
dr-xr-xr-x. 9 root root 0 Jan  21 14:49 ..
lrwxrwxrwx. 1 root root 0 Jan  21 14:44 ipc -> ipc:[3026531839]
lrwxrwxrwx. 1 root root 0 Jan  21 14:44 mnt -> mnt:[3026531840]
lrwxrwxrwx. 1 root root 0 Jan  21 14:44 net -> net:[3026531956]
lrwxrwxrwx. 1 root root 0 Jan  21 14:44 pid -> pid:[3026531836]
lrwxrwxrwx. 1 root root 0 Jan  21 14:45 user -> user:[3026531837]
lrwxrwxrwx. 1 root root 0 Jan  21 14:44 uts -> uts:[3026531838]
```
#### F. Isolation réseau

```
[root@localhost ~]$ sudo docker run -d -p 8888:7777 debian sleep 99999`
```
🌞 vérifier le réseau du conteneur

-   vérifier que le conteneur a bien une carte réseau et repérer son IP
    -   c'est une des interfaces de la _veth pair_
-   possible avec un shell dans le conteneur OU avec un `docker inspect` depuis l'hôte
```
[root@localhost ~]$ sudo docker exec -it 00225f72304c bash
root@54b56348be48:/# ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 89436 qdisc noqueue state UNKNOWN group default qlen 1000
 link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
 inet 127.0.0.1/8 scope host lo
 valid_lft forever preferred_lft forever
5: eth0@if6: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default
 link/ether 02:42:ac:11:00:02 brd ff:ff:ff:ff:ff:ff link-netnsid 0
 inet 172.17.0.4/16 brd 172.17.255.255 scope global eth0
 valid_lft forever preferred_lft forever
 ```
 🌞 vérifier le réseau sur l'hôte

-   vérifier qu'il existe une première carte réseau qui porte une IP dans le même réseau que le conteneur
```
[root@localhost ~]$ ip a
[...]
4: docker0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default
 link/ether 04:32:e7:87:45:ab bcd ff:ff:ff:ff:ff:ff
 inet 172.17.0.3/16 brd 172.17.255.255 scope global docker0
 valid_lft forever preferred_lft forever
 inet6 fe80::62:e5ff:fe67:66ab/64 scope link
 valid_lft forever preferred_lft forever
[...]
```
 -   vérifier qu'il existe une deuxième carte réseau, qui est la deuxième interface de la _veth pair_
    -   son nom ressemble à _vethXXXXX@ifXX_

```
[root@localhost ~]$ ip a
[...]
6: vethc8gff364@if5: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue master docker0 state UP group default
    link/ether 04:32:e7:87:45:ab bcd ff:ff:ff:ff:ff:ff link-netnsid 0
    inet6 fe80::f1:85ff:fe36:2c03/64 scope link
       valid_lft forever preferred_lft forever
```
identifier les règles _iptables_ liées à la création de votre conteneur
```
[root@localhost ~]$ sudo iptables -vnL
[...]
Chain DOCKER (1 references)
 pkts bytes target     prot opt in out     source destination
 0     0 ACCEPT     tcp  --  !docker0 docker0  0.0.0.0/0            172.17.0.4           tcp dpt:7777
Chain DOCKER-ISOLATION-STAGE-1 (1 references)
 pkts bytes target     prot opt in out     source destination
 0     0 DOCKER-ISOLATION-STAGE-2  all  --  docker0 !docker0  0.0.0.0/0            0.0.0.0/0
 0     0 RETURN     all  --  *      *       0.0.0.0/0            0.0.0.0/0
Chain DOCKER-ISOLATION-STAGE-2 (1 references)
 pkts bytes target     prot opt in out     source destination
 0     0 DROP       all  --  *      docker0  0.0.0.0/0            0.0.0.0/0
 0     0 RETURN     all  --  *      *       0.0.0.0/0            0.0.0.0/0
Chain DOCKER-USER (1 references)
 pkts bytes target     prot opt in out     source destination
 0     0 RETURN     all  --  *      *       0.0.0.0/0            0.0.0.0/0
[...]
```

### 2. Cgroups

#### A. Découverte manuelle

-   🌞 Lancer un conteneur Docker et déduire dans quel cgroup il s'exécute

```
[root@localhost ~]# sudo systemd-cgtop
Path                                                  Tasks   %CPU   Memory  Input/s Output/s

/                                                        71      -   552.7M        -        -
/docker                                                   -      -     1.5M        -        -
/docker/00225f72...4a6fba632143cbd344da6429d9689e8c       1      -     1.3M        -        -
/docker/087aec05...f91a868c0992032bf8ea7a64dc0e436e       1      -    44.0K        -        -
/docker/475241d3...8934f0c66a4466a914bc45292192cf34       1      -    48.0K        -        -
/docker/96d6f7a1...5d658fd8ab5982710d33bfc9644e4438       1      -    44.0K        -        -
/system.slice                                             -      -   363.8M        -        -
/system.slice/NetworkManager.service                      3      -    14.3M        -        -
/system.slice/auditd.service                              1      -     2.7M        -        -
/system.slice/containerd.service                          5      -   101.1M        -        -
/system.slice/crond.service                               1      -   760.0K        -        -
/system.slice/dbus.service                                1      -     1.7M        -        -
/system.slice/dev-mapper-centos\x2dswap.swap              -      -    60.0K        -        -
/system.slice/dev-mqueue.mount                            -      -   592.0K        -        -
/system.slice/docker.service                              1      -   141.0M        -        -
/system.slice/firewalld.service                           1      -    34.1M        -        -
/system.slice/lvm2-lvmetad.service                        1      -     1.5M        -        -
/system.slice/polkit.service                              1      -    14.9M        -        -
/system.slice/postfix.service                             3      -     6.4M        -        -
/system.slice/rsyslog.service                             1      -     2.1M        -        -
/system.slice/sshd.service                                1      -     6.7M        -        -
/system.slice/sys-kernel-debug.mount                      -      -    44.0K        -        -
/system.slice/system-getty.slice                          1      -   184.0K        -        -
/system.slice/system-getty.slice/getty@tty1.service       1      -        -        -        -
/system.slice/system-lvm2\x2dpvscan.slice                 -      -     4.0K        -        -
/system.slice/systemd-journald.service                    1      -     1.2M        -        -
/system.slice/systemd-logind.service                      1      -   952.0K        -        -
/system.slice/systemd-udevd.service                       1      -    10.7M        -        -
/system.slice/tuned.service                               1      -    13.1M        -        -
/user.slice                                               6      -   161.7M        -        -
/user.slice/user-0.slice/session-1.scope                  4      -        -        -        -


```
#### B. Utilisation par Docker

🌞 Lancer un conteneur Docker et trouver

-   la mémoire RAM max qui lui est autorisée
```
[root@localhost ~]# cat /sys/fs/cgroup/cpu/docker/96d6f7a1cffce05f01d43dd217e385445d658fd8ab5982710d33bfc9644e4438/cpu.shares
1024

```
### 3. Capabilities

#### A. Découverte manuelle

-   🌞 déterminer les capabilities actuellement utilisées par votre shell
```
[root@localhost ~]$ capsh --print
Current: =
Bounding set =cap_chown,cap_dac_override,cap_dac_read_search,cap_fowner,cap_fsetid,cap_kill,cap_setgid,cap_setuid,cap_setpcap,cap_linux_immutable,cap_net_bind_service,cap_net_broadcast,cap_net_admin,cap_net_raw,cap_ipc_lock,cap_ipc_owner,cap_sys_module,cap_sys_rawio,cap_sys_chroot,cap_sys_ptrace,cap_sys_pacct,cap_sys_admin,cap_sys_boot,cap_sys_nice,cap_sys_resource,cap_sys_time,cap_sys_tty_config,cap_mknod,cap_lease,cap_audit_write,cap_audit_control,cap_setfcap,cap_mac_override,cap_mac_admin,cap_syslog,35,36
Securebits: 00/0x0/1'b0
 secure-noroot: no (unlocked)
 secure-no-suid-fixup: no (unlocked)
 secure-keep-caps: no (unlocked)
uid=1000(root)
gid=1000(root)
groups=10(wheel),1000(root)
```
-   🌞 Déterminer les capabilities du processus lancé par un conteneur Docker
```
[root@localhost ~]$ cat /proc/2160/status | grep Cap
CapInh: 0000000000000000
CapPrm: 0000001fffffffff
CapEff: 0000001fffffffff
CapBnd: 0000001fffffffff
CapAmb: 0000000000000000
```
🌞 Jouer avec `ping`

-   trouver le chemin absolu de `ping`
```
`/usr/bin/ping`
```
récupérer la liste de ses capabilities

```
[root@localhost ~]$ getcap /usr/bin/ping
/usr/bin/ping = cap_net_admin,cap_net_raw+p
```
-   enlever toutes les capabilities
    -   en utilisant une liste vide
    -   `setcap '' <PATH>`

```
[root@localhost ~]$ sudo setcap '' /usr/bin/ping
```

-   vérifier que `ping` ne fonctionne plus

```
[root@localhost ~]$ ping 127.0.0.1
ping: socket: Operation not permitted
```

-   vérifier avec `strace` que c'est bien l'accès à l'ICMP qui a été enlevé

```
[root@localhost ~]$ strace ping 127.0.0.1
[...]
socket(AF_INET, SOCK_DGRAM, IPPROTO_ICMP) = -1 EACCES (Permission denied)
socket(AF_INET, SOCK_RAW, IPPROTO_ICMP) = -1 EPERM (Operation not permitted)
[...]
```
#### B. Utilisation par Docker

🌞 lancer un conteneur NGINX qui a le strict nécessaire de capabilities pour fonctionner

-   prouver qu'il fonctionne
````
[root@localhost proc]# sudo docker run -p 80:80 --cap-drop all --cap-add=chown --cap-add=net_bind_service --cap-add=setuid --cap-add=setgid -d nginx
e5a6c8d5502d0f97cd5c0c068511c8d7d68ad2efb7cb2ed498948671fde66ab6
[root@localhost proc]# docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                NAMES
e5a6c8d5502d        nginx               "nginx -g 'daemon of…"   6 minutes ago       Up 6 minutes        0.0.0.0:80->80/tcp   quizzical_matsumoto

````

-   expliquer toutes les capabilities dont il a besoin
 ...
